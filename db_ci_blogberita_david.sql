-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2020 at 08:19 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ci_blogberita_david`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(100) NOT NULL,
  `judul_berita` varchar(200) DEFAULT NULL,
  `berita` text DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `tanggal_publis` datetime DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `title_berita` varchar(200) DEFAULT NULL,
  `kategori` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `judul_berita`, `berita`, `slug`, `tanggal_publis`, `image`, `status`, `title_berita`, `kategori`) VALUES
(1, 'Pernyataan Lengkap Jokowi soal Bantuan Sosial untuk Hadapi Pandemi Corona', 'Jakarta - Presiden Joko Widodo (Jokowi) mengeluarkan sejumlah kebijakan bantuan sosial kepada masyarakat dalam rangka menghadapi pandemi virus Corona (COVID-19). Salah satu kebijakannya adalah memperbanyak program padat karya tunai.\r\n\"Saya telah memerintahkan agar program padat karya tunai, sekali lagi program padat karya tunai harus diperbanyak, harus dilipatgandakan dengan tetap mengikuti protokol kesehatan untuk pencegahan dan penularan COVID-19, yaitu dalam bekerja harus menjaga jarak yang aman,\" kata Jokowi dalam konferensi pers yang disiarkan langsung YouTube Sekretariat Presiden, Selasa (24/3/2020).', 'pernyataan-lengkap-jokowi-soal-bantuan-sosial-untuk-hadapi-pandemi-corona', '2020-03-26 06:00:00', 'a1.jpeg', 1, 'Pernyataan Lengkap Jokowi soal Bantuan Sosial untuk Hadapi Pandemi Corona', 1),
(2, 'China, Sumber Wabah Corona yang Kini Bantu Alat Medis ke Negara-negara Lain', 'Beijing - Beberapa pekan lalu, China masih berjuang melawan wabah virus corona, penyakit yang berawal dari Wuhan.\r\nSaat China menghadapi kondisi kritis pada Februari lalu, pemerintah di Beijing menerima bantuan masker dan peralatan medis dari hampir 80 negara dan 10 organisasi internasional.\r\n\r\nSekarang, kasus penularan baru di dalam negeri menurun drastis hingga satu digit saja, yang membuat China bisa mengalihkan sumber daya dengan mengirim bantuan ke negara-negara lain, yang kewalahan menghadapi wabah.\r\n\r\nMereka yang menerima bantuan antara lain adalah Jepang, Irak, Spanyol, Peru dan Italia, negara di Eropa yang paling parah terdampak wabah.\r\n\r\nHingga Senin (23/03) pagi, angka kematian di Italia mencapai lebih dari 5.400 orang. Secara global, lebih dari 13.000 orang meninggal dunia akibat virus ini.', 'china-sumber-wabah-corona-yang-kini-bantu-alat-medis-ke-negara-negara-lain', '2020-03-26 09:00:00', 'a2.jpeg', 1, 'China, Sumber Wabah Corona yang Kini Bantu Alat Medis ke Negara-negara Lain', 2),
(3, 'Vanessa Angel dan Suaminya Diamankan Polisi Terkait Narkoba!', 'Jakarta - Selebritis Vanessa Angel dan suaminya, yang kerap dipanggil Bibi, diamankan polisi. Keduanya ditangkap diduga terkait pemakaian narkoba.\r\n\"Iya betul yang bersangkutan kami amankan bersama suami dan asistennya,\" kata Kapolres Jakbar Audie S Latuheru kepada detikcom, Selasa (17/3/2020).\r\nVanessa dan suami beserta asisten diamankan di suatu tempat di Jakarta. Hingga saat ini ketiganya masih dalam pemeriksaan.\r\nAda barang bukti psikotropika yang diamankan.', 'vanessa-angel-dan-suaminya-diamankan-polisi-terkait-narkoba', '2020-03-27 12:00:00', 'a3.jpeg', NULL, 'Vanessa Angel dan Suaminya Diamankan Polisi Terkait Narkoba!', 1),
(4, 'Cegah Penyebaran Corona, Wakil Ketua Komisi VI DPR Usul Strategi Ini', 'Jakarta - Wakil Ketua Komisi VI DPR, Martin Manurung meminta pemerintah menutup akses masuk dan keluar Kota Jakarta. Hal itu, untuk mencegah penyebaran virus Corona (COVID-19), agar tidak semakin meluas.\r\nMartin menilai, sebagai episentrum wabah COVID-19, penutupan akses keluar masuk Jakarta sudah seharusnya dilakukan. Sebab, berdasarkan data dari laporan Gugus Tugas COVID-19, sejak terdeteksinya Corona di Indonesia, jumlah korban terus bertambah. Bahkan penyebarannya kian meluas ke berbagai daerah di nusantara.', 'cegah-penyebaran-corona-wakil-ketua-komisi-vi-dpr-usul-strategi-ini\r\n', '2020-03-29 02:53:59', 'a4.jpg', NULL, 'Cegah Penyebaran Corona, Wakil Ketua Komisi VI DPR Usul Strategi Ini', 1),
(5, 'RS Wisma Atlet Rawat 387 Pasien, 77 Orang Positif Corona', 'Jakarta, CNN Indonesia -- Rumah Sakit Darurat Wisma Atlet Kemayoran dilaporkan sedang merawat 387 pasien terkait virus corona (Covid-19) hingga Minggu (29/3) pukul 08.00 WIB.\r\n\r\n\"Dengan rincian 239 pria dan 148 Wanita,\" kata Panglima Komando Gabungan Wilayah Pertahanan (Pangkogabwilhan) I Laksamana Madya TNI Yudo Margono melalui pesan singkat, Minggu (29/3).\r\nDari data itu diketahui sebanyak 77 orang merupakan pasien positif Covid-19. Sedangkan 223 orang berstatus Pasien Dalam Pengawasan (PDP) dan 87 orang Orang Dalam Pemantauan (ODP).\r\n\r\n\"[Pasien] positif covid sebanyak 77 orang,\" ucap Yudo. Diketahui, hingga Sabtu (28/3), pemerintah pusat menyatakan terdapat 1.155 kasus positif Covid-19 di Indonesia. Sejauh ini 102 pasien dinyatakan telah meninggal dunia dan 52 pasien sembuh.', 'rs-wisma-atlet-rawat-387-pasien-77-orang-positif-corona', '2020-03-29 07:36:11', 'a5.jpeg', 1, 'RS Wisma Atlet Rawat 387 Pasien, 77 Orang Positif Corona', 1),
(6, 'Rusia Tutup Perbatasan untuk Cegah Penyebaran Virus Corona', 'Jakarta - Pemerintah Rusia akan sepenuhnya menutup perbatasan negaranya untuk mencegah penyebaran virus Corona (COVID-19). Penutupan perbatasan akan diberlakukan mulai Senin (30/3).\r\nSeperti dilansir kantor berita AFP pada Minggu (29/3/2020), keputusan menutup perbatasan diumumkan di situs resmi pemerintah Rusia. Penutupan perbatasan dilakukan sementara untuk mencegah infeksi virus Corona baru.\r\n\r\nRusia menutup perbatasannya dengan orang asing pekan lalu dan meniadakan semua penerbangan internasional pada Kamis (26/3) sebagai bagian dari tindakan semakin ketat untuk memperlambat penyebaran COVID-19 di Rusia.\r\n\r\nPejabat kesehatan Rusia telah mengkonfirmasi 1.264 kasus virus Corona dan 4 kematian, menurut perhitungan resmi hari Sabtu (28/3).\r\n\r\nDiplomat Rusia dan supir truk akan dibebaskan dari penutupan perbatasan. Warga Rusia yang tinggal secara permanen di wilayah Donetsk dan Lugansk yang memisahkan diri dari Ukraina juga masih akan diizinkan untuk melintasi perbatasan.', 'rusia-tutup-perbatasan-untuk-cegah-penyebaran-virus-corona', '2020-03-29 08:41:24', 'a6.png', NULL, 'Rusia Tutup Perbatasan untuk Cegah Penyebaran Virus Corona', 2),
(7, 'Tertinggi Dunia, 10 Ribu Orang Meninggal Gegara Corona di Italia', 'Jakarta - Italia menjadi negara yang paling parah dilanda virus Corona (COVID-19) di dunia. Lebih lebih dari 10.000 warganya meninggal dunia gegara Corona meskipun telah lockdown selama 16 hari.\r\nSeperti dilansir kantor berita AFP pada Minggu (29/3/2020) kematian baru sebanyak 889 kasus yang dilaporkan oleh Dinas Perlindungan Sipil terjadi sehari setelah negara yang berpenduduk 60 juta itu mencatat 969 kematian pada hari Jumat (27/3), jumlah korban nasional tertinggi sejak virus COVID-19 muncul akhir tahun lalu.\r\n\r\nItalia sekarang tampaknya akan memperpanjang penutupan bisnisnya yang melemahkan ekonomi dan larangan pertemuan publik melewati batas waktu 3 April.\r\n\r\n\"Apakah sudah waktunya untuk membuka kembali negara ini? Saya pikir kita harus memikirkannya dengan sangat hati-hati,\" kata Kepala Dinas Perlindungan Sipil, Angelo Borrelli kepada wartawan.\r\n\r\n\"Negara ini macet dan kita harus mempertahankan jumlah aktivitas sesedikit mungkin untuk memastikan kelangsungan hidup semua,\" sebutnya.', 'tertinggi-dunia-10-ribu-orang-meninggal-gegara-corona-di-italia', '2020-03-29 09:44:24', 'a7.jpeg', 1, 'Tertinggi Dunia, 10 Ribu Orang Meninggal Gegara Corona di Italia', 2),
(8, 'WHO Pilih Malaysia untuk Lakukan Uji Coba Obat Remdesivir untuk Virus Corona', 'Kuala Lumpur - Malaysia telah terpilih oleh Organisasi Kesehatan Dunia (WHO) sebagai salah satu negara yang akan melakukan uji coba efektivitas obat yang dinamai Remdesivir untuk mengobati para pasien COVID-19.\r\nDewan Keamanan Nasional Malaysia (NSC) menyatakan, Malaysia terpilih dikarenakan kemampuan Kementerian Kesehatan negara tersebut untuk melakukan penelitian.\r\n\r\nDirjen Kesehatan Malaysia, Dr Noor Hisham Abdullah mengatakan, Kementerian Kesehatan akan mengobati para pasien yang terinfeksi virus corona dengan obat baru, Remdesivir dan akan memonitor semua efek samping dan efektivitasnya.\r\n\r\nDalam postingan di Facebook, Noor Hisham menjelaskan bahwa pada Jumat (27/3), WHO mengumumkan uji coba global besar-besaran, yang diberi nama \"Solidarity\", untuk menemukan obat-obat mana yang bisa digunakan untuk mengobati pasien yang terinfeksi virus corona.\r\n\r\n\"Ini adalah upaya yang belum pernah terjadi sebelumnya -- upaya habis-habisan, terkoordinasi untuk mengumpulkan data ilmiah yang kuat dengan cepat selama pandemi,\" tulisnya seperti dilansir media Malaysia, The Star, Sabtu (28/3/2020).', 'who-pilih-malaysia-untuk-lakukan-uji-coba-obat-remdesivir-untuk-virus-corona', '2020-03-29 09:55:38', 'a8.jpeg', 1, 'WHO Pilih Malaysia untuk Lakukan Uji Coba Obat Remdesivir untuk Virus Corona', 2),
(9, 'Smart TV Terbaru Redmi Seukuran Meja Pingpong, Harga Rp 46 Juta', 'Jakarta - Bersamaan dengan peluncuran Redmi K30 Pro pada Selasa (24/3/2020) lalu, Redmi juga turut meluncurkan lini produk Smart TV 4K terbarunya, Smart TV Max dengan ukuran layar 98 inci. Sebagai gambaran berapa besar ukuran 98 inci itu, kurang lebih sama dengan ukuran meja pingpong (tenis meja). Layar TV super luas ini dibingkai dengan bezel berbahan stainless steel hitam yang sangat tipis. Saking tipisnya, TV pintar ini memiliki rasio layar berbanding bodi sebesar 98,8 persen. Panel layar Smart TV Max 98 inci Redmi memiliki rentang warna 85 persen NTSC dan mampu menampilkan gambar beresolusi 4K dengan refresh rate 60 Hz.', 'smart-tv-terbaru-redmi-seukuran-meja-pingpong-harga-rp-46-juta', '2020-03-29 10:11:53', 'a9.jpg', NULL, 'Smart TV Terbaru Redmi Seukuran Meja Pingpong, Harga Rp 46 Juta', 3),
(10, 'Rasanya Main Game di Layar 120 Hz Samsung Galaxy S20 Ultra', 'Jakarta - Untuk pertama kalinya, Samsung menyematkan layar dengan spesifikasi kekinian mirip ponsel gaming, di deretan ponsel Galaxy S20, Galaxy S20 Plus, dan Galaxy S20 Ultra.  Bisa dibilang begitu lantaran ketiga ponsel tersebut kompak mengusung layar berjenis Dynamic AMOLED 2X, yang memiliki refresh rate mencapai 120 Hz, berikut touch response rate hingga 240 Hz. Sekadar informasi, layar dengan refresh rate 120 Hz sejatinya mampu menampilkan animasi antar menu yang lebih mulus dibanding layar standar dengan refresh rate 60 Hz. Walhasil, proses multi-tasking pun akan terasa lebih cepat. Lantas, apakah tingkat kemulusan ini berlaku ketika ponsel digunakan untuk bermain game?\r\n', 'rasanya-main-game-di-layar-120-hz-samsung-galaxy-s20-ultra', '2020-03-29 11:16:37', 'a10.jpeg', NULL, 'Rasanya Main Game di Layar 120 Hz Samsung Galaxy S20 Ultra', 3),
(11, 'Aplikasi Pelacak Covid-19 Dirilis untuk Publik Pekan Depan', 'Jakarta - Pemerintah melalui Kementerian Komunikasi dan Informatika (Kemenkominfo) memperkenalkan aplikasi bernama \" PeduliLindungi\" untuk melacak dan mengawasi pasien Covid-19 di Indonesia. Saat diperkenalkan, aplikasi ini awalnya hanya ditujukan untuk pasien Covid-19 saja. Namun, pemerintah membuatnya menjadi aplikasi publik. Artinya, aplikasi ini bisa diunduh oleh masyarakat umum dan digunakan di smartphone siapa saja. Aplikasi ini rencananya akan tersedia mulai pekan depan. \"Mudah-mudahan Selasa atau Rabu sudah di Google Play Store dan berikutnya di Apps Store,\" kata Menteri Kominfo, Johnny G Plate, melalui pesan singkat kepada KompasTekno, Jumat (27/3/2020). Ia mengatakan, semakin banyak aplikasi ini diinstal masyarakat, maka semakin banyak pula pengumpulan data di database. Nantinya, aplikasi ini akan tersambung ke Satgas Covid-19, Kominfo, dan Kementerian Kesehatan (Kemenkes). \"Karena data-data pasien ada di Kemenkes, Kemenkes yang akan melakukan penelusuran (tracing), pelacakan (tracking), dan mengurung atau isolasi (fencing),\" jelasnya.', 'aplikasi-pelacak-covid-19-dirilis-untuk-publik-pekan-depan', '2020-03-29 11:26:35', 'a11.jpg', 1, 'Aplikasi Pelacak Covid-19 Dirilis untuk Publik Pekan Depan', 3),
(12, 'Bocoran Gambar dan Spesifikasi Ponsel Gaming Lenovo Legion', 'Jakarta - Sedikit demi sedikit bocoran mengenai smartphone gaming Lenovo Legion kian terkuak. Beberapa waktu lalu sempat hadir bocoran yang memperlihatkan aksesori dari Legion. Kini datang bocoran terbaru yang memperlihatkan sosok Legion dari sisi belakang. Dilaporkan bocoran tersebut datang dari situs kantor paten China, CNIPA.\r\n\r\nDari beberapa gambar yang ada, terlihat bahwa Lenovo Legion akan mengusung nuansa merah dan abu-abu Di punggungnya terpampang dengan jelas logo khas Legion yang ditengarai akan bisa menyala dengan lampu LED.\r\n\r\nBodi ponsel gaming Legion kabarnya bakal berlapis bahan logam. Terdapat sebuah tulisan \"cool\" yang diduga menandakan hadirnya sistem pendingin khusus pada Legion. Ada juga tulisan \"855 Snapdragon\" yang terletak tak jauh dari sisi kanan tulian \"cool\". Mungkin Legion akan dibekali dengan chip tersebut, atau bisa juga Snapdragon 865 karena ada kemungkinan gambar bocoran gambar ini merupakan versi lama.', 'bocoran-gambar-dan-spesifikasi-ponsel-gaming-lenovo-legion', '2020-03-29 12:30:03', 'a12.jpg', NULL, 'Bocoran Gambar dan Spesifikasi Ponsel Gaming Lenovo Legion', 3),
(13, 'Janji Bos UFC pada Mike Tyson hingga Ronaldo Potong Gaji', 'Bintang Juventus, Cristiano Ronaldo disebut sepakat gajinya dipotong. Total pemotongan gaji pemain asal Portugal itu mencapai 38 juta euro atau setara Rp68 miliar.\r\n\r\nDikutip dari Tuttosport, Ronaldo setuju dengan pemotongan gaji karena imbas pandemi corona. Kesepakatan itu diambil setelah CR7 bersama sang kapten tim Giorgio Chiellini dan pihak klub berdiskusi beberapa waktu lalu.\r\n\r\nPemotongan gaji Ronaldo hanya sebesar 3,8 juta euro (Rp68 miliar) dari total gaji selama setahun. Sementara gaji normalnya dia biasa menerima 31 juta euro (Rp554 miliar) per tahun.\r\n\r\nDi sisi lain, bos UFC Dana White kembali menegaskan bahwa ia bakal berusaha keras untuk menyelenggarakan duel Khabib Nurmagomedov lawan Tony Ferguson pada 18 April. Kali ini ia berjanji pada Mike Tyson.\r\n\r\nDalam wawancara dengan podcast Mike Tyson, White menyatakan ia terus berjuang keras dalam mengatur laga UFC 249 tetap berjalan sesuai jadwal.\r\n\r\n\"Saya akan berusaha menyelenggarakan Tony vs Khabib pada 18 April dan mengembalikan sejumlah agenda normal kepada negara ini,\" kata White seperti dikutip dari MMA Weekly.', 'janji-bos-ufc-pada-mike-tyson-hingga-ronaldo-potong-gaji', '2020-03-29 12:23:00', 'a13.jpeg', 1, 'Janji Bos UFC pada Mike Tyson hingga Ronaldo Potong Gaji', 5),
(14, 'Ditangkap Polisi, Jon Jones Berdalih Stres karena Corona', 'Jakarta, CNN Indonesia - Petarung top UFC, Jon Jones, mengaku stres menghadapi pandemi virus corona sehingga membuat juara kelas light heavyweight itu ditangkap polisi karena berbagai tuduhan.\r\n\r\nDalam rekaman video polisi, Jones mengatakan meminum vodka sebelum mengendarai mobil dan ditangkap pada Jumat (27/3).\r\n\r\n\"Pikiran saya kacau dan saya ingin berkendara. Ini adalah pertama kali saya berkendara dalam dua pekan,\" tutur Jones kepada polisi.\r\n\r\n\"Kemudian saya melihat ada orang-orang tuna wisma. Saya berbicara baik-baik dengan mereka, memperlakukan mereka layaknya manusia,\" sambungnya.', 'ditangkap-polisi-jon-jones-berdalih-stres-karena-corona', '2020-03-29 12:39:36', 'a14.jpeg', NULL, 'Ditangkap Polisi, Jon Jones Berdalih Stres karena Corona', 5),
(15, 'Bersitegang Sejak 2015, Marquez Masih Mengidolakan Rossi', 'Jakarta, CNN Indonesia -- Juara dunia MotoGP, Marc Marquez, mengaku masih mengidolakan Valentino Rossi kendati sudah bersaing panas dengan The Doctor sejak 2015.\r\n\r\nMarquez diketahui sebagai fan Rossi, namun persaingan di lintasan balap MotoGP justru beberapa kali menampilkan duel-duel Marquez dengan Rossi.\r\n\r\nBerbagai perseteruan dengan Rossi ternyata tak dianggap berlebihan oleh Marquez. Pebalap yang menguasai MotoGP dalam satu dasawarsa terakhir itu menganggap Rossi tetap sebagai pebalap yang layak dijadikan anutan.\r\n\r\n\"Rossi lebih baik untuk urusan gelar. Pada akhirnya Anda akan melihat gelar juara. Tetapi [Casey] Stoner memiliki bakat besar dan seperti tak memiliki batas,\" kata Marquez menjawab pertanyaan fan mengenai perbandingan antara Rossi dan Stoner dalam gelar wicara di Instagram.', 'bersitegang-sejak-2015-marquez-masih-mengidolakan-rossi', '2020-03-29 13:35:07', 'a15.jpg', NULL, 'Bersitegang Sejak 2015, Marquez Masih Mengidolakan Rossi', 5),
(16, 'Pesepak Bola Malaysia Tolak Potong Gaji karena Virus Corona', 'Jakarta, CNN Indonesia -- Asosiasi Pesepak Bola Profesional Malaysia (PFAM) menolak wacana pemotongan gaji setelah Liga Malaysia ditunda akibat pandemi corona.\r\n\r\nPenghentian kompetisi sepak bola Malaysia mencakup Malaysia Super League, Malaysia Premier League, kompetisi amatir yang dikelola Amateur Football League, Piala Presiden, hingga Piala Belia. Seluruh kompetisi ini dihentikan sampai batas waktu yang belum ditentukan.\r\n\r\nPFAM menyatakan kontrak antara pemain dan klub harus tetap dihormati meski kompetisi tengah terhenti. Hal ini mengacu kebutuhan hidup yang meningkat di tengah pandemi corona.', 'pesepak-bola-malaysia-tolak-potong-gaji-karena-virus-corona', '2020-03-29 14:48:23', 'a16.jpg', NULL, 'Pesepak Bola Malaysia Tolak Potong Gaji karena Virus Corona', 5),
(17, 'Accor Hotels Siapkan 4 Hotel untuk Tenaga Medis Covid-19', 'JAKARTA – Accor Hotels menyiapkan empat hotel untuk dijadikan akomodasi tenaga medis virus corona ( covid-19) di Jakarta.  Vice President of Sales, Marketing and Distributions untuk Accor Hotels Indonesia, Malaysia, dan Singapura, Adi Satria, menuturkan bahwa saat ini pihaknya tengah mempersiapkan Novotel Cikini, Mercure Cikini, Ibis Styles Jakarta Sunter, dan Ibis Senen. Berdasarkan siaran pers yang Kompas.com terima, Minggu (29/3/2020), dia memastikan bahwa pihaknya menerapkan Standard Operational Procedure (SOP) sesuai dengan standarisasi yang sudah ditetapkan oleh WHO dan Kementerian Kesehatan (Kemenkes). “Demi kepentingan tamu-tamu, yaitu para dokter dan perawat, serta juga menjaga kesehatan tim kami yang melayani. Mulai dari mereka datang, housekeeping, dan laundry hotel,” kata Adi. Dia menambahkan bahwa selama masa keempat hotel tersebut dijadikan sebagai sarana menginap para tenaga medis, mereka tidak membuka penginapan untuk tamu umum.', 'accor-hotels-siapkan-4-hotel-untuk-tenaga-medis-covid-19', '2020-03-29 13:56:34', 'a17.jpg', NULL, 'Accor Hotels Siapkan 4 Hotel untuk Tenaga Medis Covid-19', 4),
(18, 'Lion Air Bebaskan Biaya Penjadwalan Ulang Penerbangan', 'JAKARTA, KOMPAS.com  - Maskapai penerbangan Lion Air menetapkan kebijakan baru mengenai reschedule di tangah pandemi corona. Kebijakan ini diungkap dalam situs resmi Lion Air pada 25 Maret 2020. Menanggapi keadaan  yang tidak menentu akhir-akhir ini, pada situasi tertentu Lion Air harus membatalkan penerbangan.\r\n\r\nPihak Lion Air akan menginformasikan kepada penumpang sedini mungkin. Pihaknya akan membantu penumpang untuk mengatur penerbangan pengganti.\r\n\r\nPara penumpang diimbau untuk memantau pemberitahuan yang akan disuarakan melalui pesan teks dan email langsung kepada penumpang.', 'lion-air-bebaskan-biaya-penjadwalan-ulang-penerbangan', '2020-03-29 15:00:29', 'a18.jpg', 1, 'Lion Air Bebaskan Biaya Penjadwalan Ulang Penerbangan', 4),
(19, 'Cuma Terisi 4 Orang, Semua Penumpang Air New Zealand Ditingkatkan ke Kabin Bisnis', 'JAKARTA - Empat penumpang dalam pesawat Air New Zealand NZ283 sepertinya mendapatkan pengalaman tak terlupakan pada tanggal 25 Maret 2020 kemarin. Pasalnya keempat penumpang tersebut, ditingkatkan ke kabin kelas bisnis dari yang harusnya kelas ekonomi saat mereka terbang dari Singapura ke Auckland. Dilansir dari Simple Flying, jumlah penumpang sangat rendah karena meningkatnya pembatasan perjalanan yang diberlakukan oleh negara-negara di seluruh dunia.\r\n\r\nKebijakan itu diambil sebagai usaha menahan penyebaran wabah corona saat ini. Baca juga: AirAsia Indonesia Hentikan Sementara Seluruh Penerbangan Mulai 1 April 2020 Penerbangan NZ283 pada tanggal 25 Maret dioperasikan oleh Boeing 787-9 Dreamliner, pendaftaran ZK-NZM. Pesawat ini adalah salah satu dari 14 pesawat 787-9 yang dimiliki maskapai penerbangan Air New Zealand.\r\n\r\nPesawat ini sendiri memiliki kapasitas sampai 275 tempat duduk penumpang. Menurut News Hub, setidaknya ada 15 awak kabin yang melayani total empat penumpang dalam penerbangan tersebut.', 'cuma-terisi-4-orang-semua-penumpang-air-new-zealand-ditingkatkan-ke-kabin', '2020-03-29 00:00:00', 'a19.jpg', NULL, 'Cuma Terisi 4 Orang, Semua Penumpang Air New Zealand Ditingkatkan ke Kabin Bisnis', 4);

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(10) NOT NULL,
  `komentar` text DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `id_berita` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `komentar`, `email`, `id_berita`) VALUES
(1, 'tess', 'dadasd@gmail.com', 1),
(2, 'tesssgd', 'hhfdhsa@gmail.com', 2),
(3, 'dfdsgfdg', 'fbhas@gmail.com', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
