<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/fontawesome-free-5.13.0-web/css/all.css">

    <title> 
      <?= $title; ?>
    </title>
  
  </head>
  <body>
    <h7>Project for portfolio & practice purpose. David Surjana</h7>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href=""><h4><b>BERITAKITA</b></h4></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
        <ul class="navbar-nav mr-auto">

          <li class="nav-item active">
            <a class="nav-link" href="http://localhost/codeigniter_blogberita_david"> <h6><b>HOME</b></h6> <span class="sr-only">(current)</span></a>
          </li>

         <li class="nav-item active">
            <a class="nav-link" href="http://localhost/codeigniter_blogberita_david/blog/nasional"><h6><b>NASIONAL</b></h6></a>
          </li>

           <li class="nav-item active">
            <a class="nav-link" href="http://localhost/codeigniter_blogberita_david/blog/internasional"><h6><b>INTERNASIONAL</b></h6></a>
          </li>

          <li class="nav-item active">
            <a class="nav-link" href="http://localhost/codeigniter_blogberita_david/blog/teknologi"><h6><b>TEKNOLOGI</b></h6></a>
          </li>

          <li class="nav-item active">
            <a class="nav-link" href="http://localhost/codeigniter_blogberita_david/blog/travel"><h6><b>TRAVEL</b></h6></a>
          </li>

          <li class="nav-item active">
            <a class="nav-link" href="http://localhost/codeigniter_blogberita_david/blog/olahraga"><h6><b>OLAHRAGA</b></h6></a>
          </li>

          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
          </li> -->
          
        </ul>

        <div class="form-inline my-2 my-lg-0">
          <?= form_open('blog/search') ?>
          <input class="form-control mr-sm-2" type="text" placeholder="Search" name="keyword">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          <?= form_close() ?>
        </div>

        <!-- <form class="form-inline my-2 my-lg-0" action="" method="post">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="search" name="search">Search</button>
        </form> -->
      
      </div>
    </nav>
    