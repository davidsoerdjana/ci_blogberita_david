<div class="container"> <br>
  <div class="row">
    <div class="col-md-8">
      <!-- nama $ nya harus sama dengan nama function -->
      <h2> <?php echo $detail->judul_berita; ?> </h2>
      <img src="<?= base_url(); ?>assets/img/<?= $detail->image; ?>" class="img-thumbnail">
      <hr>
      <a> <i class="fas fa-user"></i> David Surjana | <?= $detail->tanggal_publis; ?> </a>
      <p align="justify"> <?= $detail->berita; ?> </p>

      <br>
      <h4>Komentar</h4>
      
      <!-- <?= validation_errors(); ?> bisa pakai ini atau yang dari getbootstrap -->

      <?php if (validation_errors() == TRUE): ?>
      <!-- ini yang dari getbootstrap -->
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong> <?= validation_errors(); ?> </strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <?php endif; ?>

      <form action="" method="post">
        <label>Email</label>
          <input class="form-control" type="text" name="email"></input>
        <label>Komentar</label>
          <textarea class="form-control" name="komentar"></textarea>
        <br>
        <input class="btn btn-primary" type="submit" value="Kirim" name="kirim"></input>
      </form>

      <br> <br>
      <h4>List Komentar</h4>
      <?php foreach ($komentar as $k): ?>
      <label> <?= $k->email; ?> </label> <br>
      <p> <i> <?= $k->komentar; ?> </i> </p> <br>
      <?php endforeach; ?>
      
    </div>
 