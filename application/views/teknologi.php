<div class="container"> <br>
  <div class="row">
  
    <div class="col-md-7">
    <?php foreach ($berita as $br): ?>
      <div class="row">
        
        <div class="col-md-6">
          <img src="<?= base_url(); ?>assets/img/<?= $br->image; ?>" class="img-thumbnail">
        </div>

        <div class="col-md-6">
          <a href="http://localhost/codeigniter_blogberita_david/blog/detail/<?= $br->id_berita; ?>/<?= $br->slug; ?>"> <h5> <?= $br->judul_berita; ?> </h5> </a>
          <a> <i class="fas fa-user"></i> David Surjana | <?= $br->tanggal_publis; ?> </a>
          <p> <?= word_limiter($br->berita, 15); ?> </p>
        </div>

      </div>
    <?php endforeach; ?>
    <br>
    </div>
