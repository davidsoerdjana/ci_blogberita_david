<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//1 model bisa panggil lebih dari 1 database
class Blog_m extends CI_Model { //untuk get ke database
	
	// public function __construct(){
	// 	$this->load->database();
	// }

	// function get_table(){
	// 	$query = $this->db->get('berita');
	// 	return $query->result_array();
	// }

	function get($table){
		return $this->db->get($table)->result();
	}

	function get_status(){ //penamaan functionnya juga bebas, tidak harus get_where
		return $this->db->get_where('berita', array('status' => 1))->result(); //pakai result karena data yang mau ditampilkan banyak
	}

	function get_nasional(){
		return $this->db->get_where('berita', array('kategori' => 1))->result();
	}

	function get_internasional(){
		return $this->db->get_where('berita', array('kategori' => 2))->result();
	}

	function get_teknologi(){
		return $this->db->get_where('berita', array('kategori' => 3))->result();
	}

	function get_travel(){
		return $this->db->get_where('berita', array('kategori' => 4))->result();
	}

	function get_olahraga(){
		return $this->db->get_where('berita', array('kategori' => 5))->result();
	}

	function get_detail($table, $id, $val){
		return $this->db->get_where($table, array($id => $val))->row();	//pakai row karena data yang mau ditampilkan hanya 1
	}

	function get_title(){
		$this->db->select('title_berita');
		$this->db->from('berita');
		$query = $this->db->get();

		//return $this->db->get_where('berita', 'title_berita')->row();
	}

	function insert($table, $dt){
		return $this->db->insert($table, $dt);
	}

	function get_keyword($keyword){
		$this->db->select('*');
		$this->db->from('berita');
		$this->db->like('judul_berita', $keyword);
		$this->db->or_like('berita', $keyword);
		return $this->db->get()->result();
	}

}
