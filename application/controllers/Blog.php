<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller { //1 controller bisa panggil lebih dari 1 model
	//construct digunakan  untuk pemanggilan function yang berlaku ke semua function
	function __construct(){
		parent::__construct();
		
		$this->load->model('Blog_m'); //untuk ngeload model kalo di controller penulisan nama modelnya boleh huruf kecil
		$this->load->helper(array('text')); //untuk word_limiter
		$this->load->library('form_validation');
		$this->load->helper('url');
	}

	public function index(){
		$data['berita'] = $this->Blog_m->get('berita'); //untuk database
		$data['sidebar'] = $this->Blog_m->get_status();
		$data['content'] = 'home'; 	//untuk ngeload index.php
		$data['title'] = 'Berita Terkini, Akurat & Terpercaya';
		$this->load->view('index', $data);
	}

	public function nasional(){
		$data['berita'] = $this->Blog_m->get('berita');
		$data['berita'] = $this->Blog_m->get_nasional();
		$data['sidebar'] = $this->Blog_m->get_status();
		$data['content'] = 'nasional';
		$data['title'] = 'Berita Nasional Terkini, Akurat & Terpercaya';
		$this->load->view('index', $data);
	}

	public function internasional(){
		$data['berita'] = $this->Blog_m->get('berita');
		$data['berita'] = $this->Blog_m->get_internasional();
		$data['sidebar'] = $this->Blog_m->get_status();
		$data['content'] = 'internasional';
		$data['title'] = 'Berita Internasional Terkini, Akurat & Terpercaya';
		$this->load->view('index', $data);
	}

	public function teknologi(){
		$data['berita'] = $this->Blog_m->get('berita');
		$data['berita'] = $this->Blog_m->get_teknologi();
		$data['sidebar'] = $this->Blog_m->get_status();
		$data['content'] = 'teknologi';
		$data['title'] = 'Berita Teknologi Terkini, Akurat & Terpercaya';
		$this->load->view('index', $data);
	}

	public function travel(){
		$data['berita'] = $this->Blog_m->get('berita');
		$data['berita'] = $this->Blog_m->get_travel();
		$data['sidebar'] = $this->Blog_m->get_status();
		$data['content'] = 'travel';
		$data['title'] = 'Berita Travel Terkini, Akurat & Terpercaya';
		$this->load->view('index', $data);
	}

	public function olahraga(){
		$data['berita'] = $this->Blog_m->get('berita');
		$data['berita'] = $this->Blog_m->get_olahraga();
		$data['sidebar'] = $this->Blog_m->get_status();
		$data['content'] = 'olahraga';
		$data['title'] = 'Berita Olahraga Terkini, Akurat & Terpercaya';
		$this->load->view('index', $data);
	}

	public function detail($id, $slug, $title = 'judul'){ //parameter $title nya bebas diisi apa saja
		$data['komentar'] = $this->db->query("SELECT * FROM komentar WHERE id_berita = $id")->result();
		$data['detail'] = $this->Blog_m->get_detail('berita', 'id_berita', $id);
		$data['sidebar'] = $this->Blog_m->get_status();
		$data['content'] = 'detail';
		$data['title'] = $this->Blog_m->get_title('title_berita');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email');
		$this->form_validation->set_rules('komentar', 'komentar', 'required');
		if ($this->form_validation->run() == FALSE) {

		} elseif (isset($_POST['kirim'])){
			$email = $this->input->post('email');
			$komentar = $this->input->post('komentar');
			$id_berita = $id;
			$dt = array(
				'email' => $email,
				'komentar' => $komentar,
				'id_berita' => $id_berita
			);
			$this->Blog_m->insert('komentar', $dt);
		}

		$this->load->view('index', $data);
	}

	public function search(){
		$keyword = $this->input->post('keyword');
		$data['berita'] = $this->Blog_m->get_keyword($keyword);
		//$data['title'] = 'Beritakita';
		$data['sidebar'] = $this->Blog_m->get_status();
		$data['content'] = 'home';
		$data['title'] = 'Berita Terkini, Akurat & Terpercaya';
		$this->load->view('index', $data);
	}

	// tidak terpakai
	// public function search(){
	// 	$src = $this->input->post('search');
	// 	$data['search'] = $this->db->query("SELECT * FROM berita WHERE judul_berita LIKE %src%")->result();
	// 	$data['count'] = $this->db->query("SELECT * FROM berita WHERE judul_berita LIKE %src%")->numb_rows();
		
	// 	$data['title'] = 'Beritakita';
		
	// 	$data['sidebar'] = $this->Blog_m->get_where();

	// 	$data['content'] = 'search';
	// 	$this->load->view('index', $data);
	// }

}
